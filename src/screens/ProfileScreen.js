import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
} from 'react-native';
import RemovableChips from 'react-native-chip/RemovableChips';
import SelectableChips from 'react-native-chip/SelectableChips';

export default class UserProfileView extends Component { 
    constructor(props) {
        super(props);  
    } 
  render() {
    console.log('navigation', this.props.navigation.state.params.profiledata);
    let params = this.props.navigation.state.params.profiledata; 
    let first_name = params.data.first_name; 
    let last_name = params.data.last_name;    
    let work_title =  params.data.worktitle_text;  
    let worktitle_address = params.data.worktitle_address; 
    let bio_text = params.data.bio_text;
    let FullName = `${first_name} ${last_name}`;
    let address = params.data.address; 
    let Work = `${work_title} ${worktitle_address}`;
    let experience_text = params.data.experience_text;
    let industry_text =  params.data.industry_text;
    let academic_text =  params.data.academic_text;
    let linkedin_text =  params.data.linkedin;
    let instagram_text = params.data.instagram;
    let twitter_text =  params.data.twitter;
    let website_text =  params.data.website;
    let interests = params.data.interestings_text;
    let interestsArr = interests.split(',');
    let goals = params.data.goals_text;
    let goalsArr = goals.split(',');
    let meetings = params.data.meeting_text;
    let meetingsArr = meetings.split(',');

    return (
    <ScrollView style={styles.verscroller}   > 
        <View>
            <View style={styles.container}> 
                <View style={styles.header}> 
                    <Image style={styles.backImage} source={{uri: 'https://avatars2.githubusercontent.com/u/17534788?s=460&v=4'}}/> 
                    <View style={styles.headerContent}> 
                        <Image style={styles.avatar} source={{uri: 'https://avatars2.githubusercontent.com/u/17534788?s=460&v=4'}}/> 
                        < Text style={styles.name}>{FullName}</Text>
                        <Text style={styles.userInfo}>{Work}</Text>
                        <Text style={styles.userInfo}>{address}</Text>
                    </View>
                </View> 
                <View style={styles.body}>  
                    <Text style={styles.info}>Interests</Text>
                    <ScrollView style={styles.scroller} showsHorizontalScrollIndicator={false} horizontal={true}>
                        <SelectableChips  chipStyle={styles.chip}  initialChips={interestsArr} alertRequired={false}/>
                    </ScrollView>
                    <View style={styles.divider} /> 
                    <Text style={styles.info}>Goals</Text>
                    <ScrollView style={styles.scroller} showsHorizontalScrollIndicator={false} horizontal={true}>
                        <SelectableChips  chipStyle={styles.chip}  initialChips={goalsArr}  alertRequired={false}/>
                    </ScrollView>
                    <View style={styles.divider} /> 
                    <Text style={styles.info}>Bio</Text>
                    <Text style={styles.info}>{bio_text}</Text>  
                    <View style={styles.divider} /> 
                    <Text style={styles.info}>Favourite Ways To Meet</Text>
                    <ScrollView style={styles.scroller} showsHorizontalScrollIndicator={false} horizontal={true}>
                        <SelectableChips  chipStyle={styles.chip}  initialChips={meetingsArr} alertRequired={false}/>
                    </ScrollView>
                    <Text style={styles.info}>Experience</Text> 
                    <View style={styles.experBlog}> 
                        <View  style={styles.exper} >
                            <Text style={styles.info}>Experience : </Text>
                            <Text style={styles.userinfo}>{experience_text}</Text>
                        </View>
                        <View style={styles.exper}>
                            <Text style={styles.info}>My Industry : </Text>
                            <Text style={styles.userinfo}>{industry_text}</Text>
                        </View>
                        <View style={styles.exper}>
                            <Text style={styles.info}>Previous Organizations : </Text> 
                        </View>
                        <View style={styles.exper}>
                            <Text style={styles.info}>Academic Level : </Text> 
                            <Text style={styles.userinfo}>{academic_text}</Text>
                        </View> 
                        <View style={styles.social} > 
                            <View style={styles.exper}>
                                <Text style={styles.info}>LinkedIn : </Text>
                                <Text style={styles.userinfo}>{linkedin_text}</Text>
                            </View>
                            <View style={styles.exper}>
                                <Text style={styles.info}>Instagram : </Text>
                                <Text style={styles.userinfo}>{instagram_text}</Text>
                            </View>
                            <View style={styles.exper}>
                                <Text style={styles.info}>Twitter : </Text>
                                <Text style={styles.userinfo}>{twitter_text}</Text>
                            </View>
                            <View style={styles.exper}>
                                <Text style={styles.info}>Website : </Text>
                                <Text style={styles.userinfo}>{website_text}</Text>
                            </View>  
                        </View> 
                    </View>  
                </View>   
            </View>
        </View>
    </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    backgroundColor: "#ffffff",
    alignItems: 'center',
  },
  headerContent:{
    padding:30,
    alignItems: 'center',
    position: 'absolute'
  },
  backImage: {
    alignSelf: 'stretch',
    height: 250, 
  },
  chip: {
    backgroundColor: "#a7d4f2",  
    height: 28,  
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 1,
    borderColor: "white",
    marginBottom:10,
  },
  divider:{
    alignSelf: 'stretch',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    margin : 20
  },
  name:{
    fontSize:22,
    color:"#ffffff",
    fontWeight:'600',
  },
  userInfo:{
    fontSize:16,
    color:"#ffffff",
    fontWeight:'200',
  },
  body:{
    backgroundColor: "#ffffff",
    // height:'100%',
    alignItems:'center' 
  },
  item:{
    flexDirection : 'row',
  },
  infoContent:{
    flex:1,
    alignItems:'flex-start',
    paddingLeft:5
  },
  iconContent:{
    flex:1,
    alignItems:'flex-end',
    paddingRight:5,
  },
  icon:{
    width:30,
    height:30,
    marginTop:20,
  },
  info:{
    fontSize:15, 
    color: "#2268A9", 
    textAlign: 'left'
  },
  userinfo:{
    fontSize:15, 
    color: "#000000", 
    textAlign: 'center'
  },
  scroller:{
    height: 40,  
  },
  verscroller:{ 
    width: '100%', 
    height :'100%'
  },
  social:{ 
    backgroundColor :"#F7F7F7",
    width: '100%', 
  },
  exper:{ 
    flexDirection: 'row',
    alignItems: 'flex-start', 
    padding: 10,
  },
  experBlog:{
    alignItems: 'flex-start', 
    width: '100%', 
    
  }, 
});
 