import React , {useState} from 'react';
import { Text, StyleSheet , View ,ScrollView ,TextInput ,Button ,ToastAndroid} from 'react-native';
import axios from 'axios'; 
 
const RegisterScreen = props => {
  const [username , setUsername] =  useState(''); 
  const [password , setPassword] =  useState(''); 
  const [Repassword , setRepassword] =  useState(''); 
  const [FirstName , setFirstName] =  useState(''); 
  const [LastName , setLastName] =  useState(''); 
  const [email , setEmail] =  useState(''); 
  const [result , setResult] = useState('') ; 
  const [error,setError]= useState('') ; 

  const emailcheck = (email) => {
    const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

    return expression.test(String(email).toLowerCase())
  }
 
  registerApi = async() => {
    try{  
      let bodyFormData = new FormData();
  //    bodyFormData.append('access_token', '0d5bce0009fae1088266f892d5bc45925a82dacc748933866f2ccfdfa2a4f2de0ae50d9121805123b0ba5c44aaf65f6ca34cf116e6d82ebf'); 
      bodyFormData.append('username',username),
      bodyFormData.append('first_name',FirstName),
      bodyFormData.append('last_name',LastName),
      bodyFormData.append('email',email),
      bodyFormData.append('mobile_device_id','sdfgbsdbsdgv sdvgdvsdvsdbsbsdbsdb'),
      bodyFormData.append('password',password); 
      axios.post('http://app.tech-solt.com/endpoint/v1/27ce3f02d98d388b9021021f74ffe702/users/register', bodyFormData)
        .then(function (response) {
            //handle success
            //
            setResult(response.status);

           console.log('signup',result);
        })
        .catch(function (err) {
            //handle error
            console.log(err);
            setError(err);
        });   
    }catch(err){
      console.log(err);
    }
  }; 
  registercheck = ( ) => {  
    if (username && LastName && FirstName && email  && password && Repassword ){ 
      if (emailcheck(email)){
        if (password == Repassword){
          registerApi();
          if (result == 200){
            ToastAndroid.show('A pikachu appeared nearby !', ToastAndroid.SHORT);
          }
          else{
            ToastAndroid.show(error, ToastAndroid.SHORT);
          }
        }
        else {
          ToastAndroid.show('Password Doesnt Match', ToastAndroid.SHORT);
        }  
      } 
      else{
        ToastAndroid.show('Please Enter Valid Email', ToastAndroid.SHORT);
      }
    }
    else {
      ToastAndroid.show('Please Enter The Missing Values', ToastAndroid.SHORT);
    }
  } 

  return(
  <View>
    <ScrollView style={{padding: 20}}>
      <Text 
          style={{fontSize: 27}}>
          Register Screen
      </Text>
      <TextInput style={{margin:50},{fontSize: 27}} placeholder='FirstName' name='FirstName' onChangeText={(FirstName) => setFirstName(FirstName)} value={FirstName}/>
      <View style={{margin:7}} />
      <TextInput style={{margin:50},{fontSize: 27}} placeholder='Last Name' name='LastName' onChangeText={(LastName) => setLastName(LastName)} value={LastName}/>
      <View style={{margin:7}} />
      <TextInput style={{margin:50},{fontSize: 27}} placeholder='Email' name='email' onChangeText={(email) => setEmail(email)} value={email}/>
      <View style={{margin:7}} />
      <TextInput style={{margin:50},{fontSize: 27}} placeholder='Username' name='username' onChangeText={(username) => setUsername(username)} value={username} />
      <View style={{margin:7}} />
      <TextInput style={{margin:50},{fontSize: 27}} placeholder='Password' name='password' onChangeText={(password) => setPassword(password)} value={password}/>
      <View style={{margin:7}} />
      <TextInput style={{margin:50},{fontSize: 27}} placeholder='Retype Password' name='Repassword' onChangeText={(Repassword) => setRepassword(Repassword)} value={Repassword}/>
      <View style={{margin:7}} /> 
      <Button  
          title="Register"
          onPress={ this.registercheck} 
      />
    </ScrollView>
  </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default RegisterScreen;
