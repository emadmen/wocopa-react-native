import React, {useState} from 'react';
import { Text, StyleSheet , View ,ScrollView ,TextInput ,Button ,ToastAndroid} from 'react-native'; 
import yelp from './api/yelp' 
import axios from 'axios'; 

const LoginScreen = props => { 
  const [username , setUsername] =  useState(''); 
  const [password , setPassword] =  useState(''); 
  const [result , setResult] = useState('') ; 
  const [secresult , setSecResult] = useState('') ; 
  const [AccesToken , setAccesToken] = useState('') ; 
  const [userId , setUserId] = useState('') ; 
  const [error,setError]= useState('') ; 
  const [Profiledata,setProfiledata]= useState([]) ;   

  profileApi = () => { 
      let bodyFormData = new FormData(); 
      console.log(AccesToken);
      bodyFormData.append('access_token',AccesToken),
      bodyFormData.append('user_id',userId),
      bodyFormData.append('fetch','data'),
      bodyFormData.append('device_id','sdfgbsdbsdgv sdvgdvsdvsdbsbsdbsdb'); 
      return new Promise((resolve, reject) => {
        axios.post('http://app.tech-solt.com/endpoint/v1/27ce3f02d98d388b9021021f74ffe702/users/profile', bodyFormData)
          .then(function (response) {   
            setSecResult(response.status); 
            setProfiledata(response.data);  
            resolve();
          })
          .catch(function (err) {
              //handle error
          //    
              setError(err);
              console.log(err);
              reject();
          });   

      })
  }; 

  // loginApi = () => {  
  //     let bodyFormData = new FormData();
  // //    bodyFormData.append('access_token', '0d5bce0009fae1088266f892d5bc45925a82dacc748933866f2ccfdfa2a4f2de0ae50d9121805123b0ba5c44aaf65f6ca34cf116e6d82ebf'); 
  //     bodyFormData.append('username',username),
  //     bodyFormData.append('platform','mobile'),
  //     bodyFormData.append('device_id','sdfgbsdbsdgv sdvgdvsdvsdbsbsdbsdb'),
  //     bodyFormData.append('password',password),
  //     axios.post('http://app.tech-solt.com/endpoint/v1/27ce3f02d98d388b9021021f74ffe702/users/login', bodyFormData)
  //       .then(function (response) { 
  //           setResult(response.status);
  //           setAccesToken(response.data.data.access_token); 
  //           setUserId(response.data.data.user_id) ; 
  //       })
  //       .catch(function (err) {
  //           //handle error
  //         //  console.log(err);
  //           setError(err);
  //       }); 
  // }; 
  loginApi = () => {  
      let bodyFormData = new FormData();
  //    bodyFormData.append('access_token', '0d5bce0009fae1088266f892d5bc45925a82dacc748933866f2ccfdfa2a4f2de0ae50d9121805123b0ba5c44aaf65f6ca34cf116e6d82ebf'); 
      bodyFormData.append('username',username),
      bodyFormData.append('platform','mobile'),
      bodyFormData.append('device_id','sdfgbsdbsdgv sdvgdvsdvsdbsbsdbsdb'),
      bodyFormData.append('password',password);
      return new Promise((resolve, reject) => {
        axios.post('http://app.tech-solt.com/endpoint/v1/27ce3f02d98d388b9021021f74ffe702/users/login', bodyFormData)
        .then(function (response) { 
            // console.log(response);
            setResult(response.status); 
            setAccesToken(response.data.data.access_token); 
            setUserId(response.data.data.user_id) ; 
            resolve();
        })
        .catch(function (err) {
            //handle error
          //  console.log(err);
            setError(err);
            reject();
        });
      })
       
  }; 

  setprofilefunc =()=> {
    try{
      // props.navigation.navigate('Profile', {
      //   profiledata : this.Profiledata, });
      //  setProfiledata(response); 
      console.log('my data' ,Profiledata);
      props.navigation.navigate('Profile', {
         profiledata : Profiledata, });
    }
    catch(err){
      console.log(err);
    }
    
  }
  logincheck = ( ) => {
    loginApi()
      .then(() => {
        ToastAndroid.show('A pikachu appeared nearby !', ToastAndroid.SHORT); 
        profileApi()
          .then(() => {
            ToastAndroid.show('Profile Data Fetched!', ToastAndroid.SHORT);
            setprofilefunc(); 
            // props.navigation.navigate('Profile', {
            // profiledata : this.Profiledata, });
            
          })
          .catch(err => {
            console.log(err);
            return;
          });
        
      })
      .catch(err => {
        console.log(err);
        ToastAndroid.show('New Error Missing Values', ToastAndroid.SHORT);
        return;
      })
      console.log(Profiledata)
      // const {username }= useState;
    // const {password }= useState;
    // console.log(password); 

    // if (username && password ){ 
    //   if (result == 200){
        
    //     if (secresult == 200){
    //       // ToastAndroid.show('Profile Data Fetched!', ToastAndroid.SHORT);
    //        props.navigation.navigate('Profile', {
    //         profiledata: Profiledata, });
    //     } 
    //     else {
    //       ToastAndroid.show('Profile Data  Can not Be Fetched!', ToastAndroid.SHORT);
    //     }
    //   }
    //   else{
    //     ToastAndroid.show('New Error Missing Values', ToastAndroid.SHORT);
    //   }
    // }
    // else {
    //   ToastAndroid.show('Please Enter The Missing Values', ToastAndroid.SHORT);
    // }
  }  
  return(
    <View>
    <ScrollView style={{padding: 20}}>
      <Text 
          style={{fontSize: 27}}>
          Login Screen
      </Text>
      <TextInput style={{margin:10,fontSize: 18}} placeholder='Username' id="username" name='username' onChangeText={(username) => setUsername(username)} value={username}  />
      <View style={{margin:7}} />
      <TextInput style={{margin:10,fontSize: 18}} placeholder='Password'  id="password" name='password' onChangeText={(password) => setPassword(password)} value={password} />
      <View style={{margin:7}} />
      <Button
      style={{margin:20,fontSize: 27,borderRadius :10}} 
        //  onPress={ () => this.props.navigation.navigate('Register')}  
         onPress={ this.logincheck}    
        title="Login"
      />
      <View style={{margin:7}} />
      <Button  style={{}} onPress={ () => props.navigation.navigate('Register') } title="Register" />
      <View style={{margin:7}} />
      <Button 
        //  onPress={ this.profileApi}    
         style={styles.text}
        title="LoginApi"
      />
      <Text 
          style={{fontSize: 27}}>fdsf</Text>
      <View style={{margin:7}} />  
    </ScrollView>
  </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    backgroundColor:'#c90825',
    fontSize: 18    
  }
});

export default LoginScreen;