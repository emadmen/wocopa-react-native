import React, {Component} from 'react';
import { Text, StyleSheet , View ,ScrollView ,TextInput ,Button ,ToastAndroid} from 'react-native'; 
// import yelp from '../../api/yelp' 
 

class LoginScreen extends Component{
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    } 
  }  
  // const [result , setResult] = useState([]) ;

  // loginApi = async() => {
  //   const response = await yelp.get('/search',{
  //     param : {
  //       limit :50
  //     }
  // });
  // setResult(response.data.businesses)
// };
  logincheck = ( ) => {
    const {username }= this.state;
    const {password }= this.state;
    console.log(this.props);
    if (username && password ){
      ToastAndroid.show('A pikachu appeared nearby !', ToastAndroid.SHORT);
    }
    else {
      ToastAndroid.show('Please Enter The Missing Values', ToastAndroid.SHORT);
    }
  } 
  render(){
    const {username, password} = this.state;
      return(
      <View>
        <ScrollView style={{padding: 20}}>
          <Text 
              style={{fontSize: 27}}>
              Login Screen
          </Text>
          <TextInput style={{margin:50},{fontSize: 27}} placeholder='Username' id="username" name='username' onChangeText={(username) => this.setState({username})} value={username}/>
          <View style={{margin:7}} />
          <TextInput style={{margin:50},{fontSize: 27}} placeholder='Password'  id="password" name='password' onChangeText={(password) => this.setState({password})} value={password}/>
          <View style={{margin:7}} />
          <Button
            //  onPress={ () => this.props.navigation.navigate('Register')}  
              onPress={ this.logincheck}    
            title="Login"
          />
          <View style={{margin:7}} />
          <Button  
            onPress={ () => 
              this.props.navigation.navigate('Register') 
          }   
            title="Register"
          />
        </ScrollView>
      </View>
      );
    };

  }
 

// const LoginScreen = props => {

//   return(
//   <View>
//     <ScrollView style={{padding: 20}}>
//       <Text 
//           style={{fontSize: 27}}>
//           Login Screen
//       </Text>
//       <TextInput style={{margin:50},{fontSize: 27}} placeholder='Username' id="username" />
//       <View style={{margin:7}} />
//       <TextInput style={{margin:50},{fontSize: 27}} placeholder='Password'  id="password" />
//       <View style={{margin:7}} />
//       <Button
//         //  onPress={ () => props.navigation.navigate('Main')}   
//         // onPress={ () =>  emailcheck()}   
//         title="Login"
//       />
//       <View style={{margin:7}} />
//       <Button  
//         onPress={ () => props.navigation.navigate('Register')}   
//         title="Register"
//       />
//     </ScrollView>
//   </View>
//   );
// };

// const styles = StyleSheet.create({
//   text: {
//     fontSize: 30
//   }
// });

export default LoginScreen;
