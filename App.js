import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './src/screens/HomeScreen';
import LoginScreen from  './src/screens/login';
import RegisterScreen from './src/screens/RegisterScreen';
import MainScreen from './src/screens/MainScreen';
import ProfileScreen from './src/screens/ProfileScreen';

const navigator = createStackNavigator(
  {
    Home: HomeScreen, 
    Login : LoginScreen,
    Register :RegisterScreen,
    Main : MainScreen ,
    Profile : ProfileScreen
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
    navigationOptions: {  
    }
  }
);

export default createAppContainer(navigator);
